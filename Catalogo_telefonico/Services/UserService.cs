﻿using AutoMapper;
using Catalogo_telefonico.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Catalogo_telefonico.Repository;
using Catalogo_telefonico.Models.Enums;

namespace Catalogo_telefonico.Services
{
    public class UserService
    {
        private readonly UserRepository _repository;
       public UserService(UserRepository repository)
        {
            _repository = repository;
        }
        public async Task<ServiceResult<SccUser>> save(RegisterModel model)
        {
            SccUser user = Mapper.Map<SccUser>(model);
            var result = await _repository.SaveAsync(user, model.Password, model.roles);
            return new ServiceResult<SccUser>(result.Succeeded,
                                              message: result.Succeeded ?
                                                       "Usuario creado." :
                                                       result.Errors.FirstOrDefault().Description);
        }
        internal async Task<ServiceResult<SccUser>> Delete(string id)
        {
            var user = await _repository.GetByIdAsync(id);
            if (user != null)
            {
                var result = await _repository.DeleteAsync(user);
                return new ServiceResult<SccUser>(result.Succeeded,
                                                  message: result.Succeeded ?
                                                           "Usuario deshabilitado." :
                                                           result.Errors.FirstOrDefault().Description);
            }
            return new ServiceResult<SccUser>(false, message: "No se encontró usuario");
        }
        internal async Task<ServiceResult<SccUser>> Edit(UserModel model)
        {
            SccUser user = await _repository.GetByIdAsync(model.Id);
            if (user != null)
            {
                user.name = model.name;
                user.UserName = model.UserName;
                user.telefono = model.telefono;
                user.tipoUser = model.tipoUser;

                var result = await _repository.EditAsync(user);
                var userRoles = await _repository.GetRoles(user);

                if (result.Succeeded && !userRoles.Contains(model.roles.ToString()))
                {
                    result = await _repository.ChangeRole(user, userRoles.FirstOrDefault(), model.roles);
                }
                return new ServiceResult<SccUser>(result.Succeeded,
                                                  message: result.Succeeded ?
                                                           "Usuario modificado." :
                                                           result.Errors.FirstOrDefault().Description);
            }
            return new ServiceResult<SccUser>(false, message: "No se encontró usuario.");
        }

        public async Task<ServiceResult<UserModel>> Get(string Id = "", string Name = "")
        {
            SccUser user = string.IsNullOrEmpty(Id) ?
                            await _repository.GetByNameAsync(Name) :
                            await _repository.GetByIdAsync(Id);

            if (user != null)
            {
                var roles = await _repository.GetRoles(user);
                ERolesView userRole = (ERolesView)Enum.Parse(typeof(ERolesView), roles.FirstOrDefault());
                UserModel model = Mapper.Map<UserModel>(user);
                model.roles = userRole;

                return new ServiceResult<UserModel>(true, model);
            }
            return new ServiceResult<UserModel>(false, message: "No se encontró usuario.");
        }
    }
}
