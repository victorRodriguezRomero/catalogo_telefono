﻿using Catalogo_telefonico.Models;
using Catalogo_telefonico.Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Services
{
    public class AuthorizationService:Controller
    {
        private readonly UserManager<SccUser> _userManager;

        public AuthorizationService(UserManager<SccUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IActionResult> RedirectUser(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (await _userManager.IsInRoleAsync(user, Roles.Administrador.ToString()))
            {
                return RedirectToAction("Index", "Administrador");
            }
            if (await _userManager.IsInRoleAsync(user, Roles.Telefonico.ToString()) || await _userManager.IsInRoleAsync(user, Roles.supervisor.ToString()))
            {
                return RedirectToAction("Index", "LineasMoviles");
            }
            return RedirectToAction("");
        }
    }
}
