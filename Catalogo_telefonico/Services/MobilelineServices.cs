﻿using Catalogo_telefonico.Models;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;

namespace Catalogo_telefonico.Services
{
    public class MobilelineServices
    {
       public List<Mobileline> GetMobileline(string name)
        {
            List<Mobileline> line = new List<Mobileline>();
            var fileRoot = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + name;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileRoot, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
                {
                    int row = 0;
                    while (reader.Read())
                    {
                        if (row < 1)
                        {
                            row++;
                        }
                        else
                        {
                            var phone = reader.GetValue(1).ToString();
                            var description = reader.GetValue(2).ToString();
                            line.Add(new Mobileline
                            {
                                Phone = phone,
                                Name = description
                            });
                            row++;
                        }
                    }
                }
            }
            return line;
        }

        internal List<DetailMobileline> GetDetailMobileline(string fileName)
        {
            List<DetailMobileline> line = new List<DetailMobileline>();
            var fileRoot = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fileName;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileRoot, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    int row = 0;
                    while( reader.Read())
                    {
                        if (row < 1)
                        {
                            row++;
                        }
                        else
                        {
                            var phone = reader.GetValue(1).ToString();
                            var Numberdescription = reader.GetValue(2).ToString();
                            var lugarMarcado = reader.GetValue(3).ToString();
                            var fecha = reader.GetValue(4).ToString();
                            var duration = int.Parse(reader.GetValue(5).ToString());
                            decimal totalcost = Convert.ToDecimal(reader.GetValue(6).ToString());
                            DateTime fech = DateTime.Parse(fecha);

                            line.Add(new DetailMobileline
                            {
                                Phone = phone,
                                CalledPartyNumber = Numberdescription,
                                CalledPartyDescription = lugarMarcado,
                                DateTimeS =fech,
                                Duration = duration,
                                TotalCost = totalcost

                            });
                            row++;
                        }
                    }
                }
            }
            return line;
        }
    }
}
