﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Help
{
    public class Paginacion<t> : List<t>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; set; }

        public Paginacion(List<t> items, int count, int pageindex, int pagesize)
        {
            PageIndex = pageindex;
            TotalPages = (int)Math.Ceiling(count / (double)pagesize);
            this.AddRange(items);
        }
        public bool PreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }
        public bool NextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }
        public static async Task<Paginacion<t>> Create(IQueryable<t> source, int PageIndwx, int PageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((PageIndwx - 1) * PageSize).Take(PageSize).ToListAsync();
            return new Paginacion<t>(items, count, PageIndwx, PageSize);

        }
    }
}
