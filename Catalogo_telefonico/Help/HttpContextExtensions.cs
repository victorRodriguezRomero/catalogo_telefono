﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Help
{
    public static class HttpContextExtensions
    {
        public static async Task InsertParamResponsePages<T>(this HttpContext context,
           IQueryable<T> queryable, int CountRegisterShow)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            double count = await queryable.CountAsync();
            double totalCountpages = Math.Ceiling(count / CountRegisterShow);
            context.Response.Headers.Add("total de paginas", totalCountpages.ToString());


        }
    }
}
