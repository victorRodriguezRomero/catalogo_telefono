﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [mobilelines] (
    [phone] varchar(10) NOT NULL,
    [id] int NOT NULL IDENTITY,
    [name] varchar(50) NULL,
    CONSTRAINT [PK__mobileli__B43B145E37DE89AA] PRIMARY KEY ([phone])
);
GO

CREATE TABLE [user] (
    [id] int NOT NULL IDENTITY,
    [nombre] nvarchar(50) NOT NULL,
    [tipoUser] int NOT NULL,
    [roles] int NOT NULL,
    [IsActive] bit NOT NULL,
    [Id] nvarchar(max) NULL,
    [UserName] nvarchar(max) NULL,
    [NormalizedUserName] nvarchar(max) NULL,
    [Email] nvarchar(max) NULL,
    [NormalizedEmail] nvarchar(max) NULL,
    [EmailConfirmed] bit NOT NULL,
    [PasswordHash] nvarchar(max) NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [LockoutEnabled] bit NOT NULL,
    [AccessFailedCount] int NOT NULL,
    CONSTRAINT [PK_user] PRIMARY KEY ([id])
);
GO

CREATE TABLE [detailMobilelines] (
    [CallDetailId] int NOT NULL IDENTITY,
    [Phone] varchar(10) NULL,
    [CalledPartyNumber] varchar(12) NULL,
    [CalledPartyDescription] varchar(20) NULL,
    [DateTimeS] datetime NULL,
    [Duration] int NULL,
    [TotalCost] decimal(18,0) NULL,
    CONSTRAINT [PK__detailMo__B1DC93BE2DAF09E7] PRIMARY KEY ([CallDetailId]),
    CONSTRAINT [FK__detailMob__Phone__267ABA7A] FOREIGN KEY ([Phone]) REFERENCES [mobilelines] ([phone]) ON DELETE NO ACTION
);
GO

CREATE INDEX [IX_detailMobilelines_Phone] ON [detailMobilelines] ([Phone]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210306021831_initial', N'5.0.3');
GO

COMMIT;
GO

