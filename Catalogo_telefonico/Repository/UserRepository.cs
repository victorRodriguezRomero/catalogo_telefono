﻿using Catalogo_telefonico.Models;
using Catalogo_telefonico.Models.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Repository
{
    public class UserRepository
    {
        private readonly UserManager<SccUser> _userManager;
       public UserRepository(UserManager<SccUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<SccUser> GetByNameAsync(string name)
        {
            var user = await _userManager.FindByNameAsync(name);
            return user;

        }
        public async Task<SccUser> GetByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return user;
        }

        public async Task<IdentityResult> DeleteAsync(SccUser obj)
        {
            var result = await _userManager.DeleteAsync(obj);
            return result.Succeeded ?
                    await _userManager.SetLockoutEndDateAsync(obj, DateTimeOffset.MaxValue) :
                    result;
        }
        public async Task<IdentityResult> EditAsync(SccUser obj)
        {
            return await _userManager.UpdateAsync(obj);
        }
        public async Task<IdentityResult> ChangeRole(SccUser user, string lastRole, ERolesView newRole)
        {
            var result = await _userManager.RemoveFromRoleAsync(user, lastRole);
            if (result.Succeeded)
            {
                result = await _userManager.AddToRoleAsync(user, newRole.ToString());
            }
            return result;
        }

        public async Task<IEnumerable<string>> GetRoles(SccUser user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> SaveAsync(SccUser obj, string password, Roles role)
        {
            var comparar = _userManager.Users.Select(c=>c.telefono==obj.telefono);
            var result = await _userManager.CreateAsync(obj, password);
            if (comparar.Count() == 0)
            {
                if (result.Succeeded)
                {
                    SccUser user = await GetByNameAsync(obj.UserName);
                    string prueba = role.ToString();
                    result = await _userManager.AddToRoleAsync(user, role.ToString());
                }
            }
            return result;
        }

    }
}
