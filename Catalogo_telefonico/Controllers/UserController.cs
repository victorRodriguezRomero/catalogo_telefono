﻿using Catalogo_telefonico.Models;
using Catalogo_telefonico.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Controllers
{
    public class UserController:Controller
    {
        private readonly UserService _user;
        private readonly UserManager<SccUser> _usermanager;
        public UserController(UserService userService, UserManager<SccUser> user)
        {
            _user = userService;
            _usermanager = user;

        }

        public ActionResult create()
        {
            return View();
        }
        [HttpPost("create")]
        public async Task<IActionResult> Create(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _user.save(model);
  
                if (result.Success)
                {
                    return RedirectToAction(nameof(AdministradorController.Index), "Administrador");
                }
                TempData["create-error"] = result.Message.ToString();

            }
            return View(model);
        }
        public async Task<IActionResult> Edit(string Id)
        {
            var result = await _user.Get(Id);
            if (result.Success)
            {
                return View(result.Data);
            }
            TempData["UserMessage"] = result.Message;
            return RedirectToAction(nameof(AdministradorController.Index), "Administrador");
        }
        [HttpPost]
        public async Task<IActionResult> Edit(UserModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _user.Edit(model);
                TempData["UserMessage"] = result.Message;
                if (result.Success)
                {
                    return RedirectToAction(nameof(AdministradorController.Index), "Administrador");
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Delete(string Id)
        {
            var result = await _user.Delete(Id);
            if (result.Success)
            {
                return RedirectToAction(nameof(AdministradorController.Index), "Administrador");
            }
            TempData["deleteerror"] = result.Message;
            return RedirectToAction(nameof(AdministradorController.Index), "Administrador");
        }
    }
}
