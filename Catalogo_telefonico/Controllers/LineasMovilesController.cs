﻿using Catalogo_telefonico.Help;
using Catalogo_telefonico.Models;
using Catalogo_telefonico.Models.ViewModel;
using Catalogo_telefonico.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Controllers
{

    public class LineasMovilesController : Controller
    {
        private readonly LineasContext _context;
        private readonly MobilelineServices _MobilelineServices;
        public LineasMovilesController(LineasContext context, MobilelineServices mobilelineServices)
        {
            _context = context;
            _MobilelineServices = mobilelineServices;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Index(string searchString, int pageNumber = 1)
        {
            ViewData["CurrentFilter"] = searchString;
            IQueryable<Mobileline> listaMovil = _context.Mobilelines.OrderBy(c => c.Phone).AsQueryable();


            if (!String.IsNullOrEmpty(searchString))
            {
                listaMovil = listaMovil.Where(c => c.Phone.Contains(searchString) || c.Name.Contains(searchString));
            }

            return View(await Paginacion<Mobileline>.Create(listaMovil, pageNumber, 5));

        }
        public async Task<ActionResult<DetailMobileline>> Detail(string valor, int pageNumber = 1)
        {
            IQueryable<DetailMobileline> detailMobilelines = _context.DetailMobilelines.Where(c => c.Phone == valor).OrderBy(c => c.DateTimeS).AsQueryable();
            TempData["Value"] = valor;
            return View(await Paginacion<DetailMobileline>.Create(detailMobilelines, pageNumber, 5));
        }
        public ActionResult ImportMobileline()
        {
            //get the selected radio value
            return View();
        }
        
       [HttpPost]
       public ActionResult ImportMobileline(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            if (ModelState.IsValid)
            {
                var fileRoot = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
                using (FileStream fileStream = System.IO.File.Create(fileRoot))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
                List<Mobileline> lines = _MobilelineServices.GetMobileline(file.FileName);
                foreach (var line in lines)
                {
                    _context.Mobilelines.Add(new Mobileline
                    {
                        Phone = line.Phone,
                        Name = line.Name
                    });
                }
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ImportdetailLine(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            var filedetailRoot = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
            if (System.IO.File.Exists(filedetailRoot)){
                TempData["Exist"] = "El archivo existe";
                System.IO.File.Delete(filedetailRoot);
            }
            else
            {
                using (FileStream fileStream = System.IO.File.Create(filedetailRoot))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
                List<DetailMobileline> DetaiLines = _MobilelineServices.GetDetailMobileline(file.FileName);

                foreach (var detal in DetaiLines)
                {
                    _context.DetailMobilelines.Add(new DetailMobileline
                    {
                      Phone=detal.Phone,
                      CalledPartyNumber=detal.CalledPartyNumber,
                      CalledPartyDescription=detal.CalledPartyDescription,
                      DateTimeS=detal.DateTimeS,
                      Duration=detal.Duration,
                      TotalCost=detal.TotalCost
                    });
                }
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
    }

