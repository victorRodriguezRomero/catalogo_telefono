﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalogo_telefonico.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using Catalogo_telefonico.Models.Enums;
using Catalogo_telefonico.Help;

namespace Catalogo_telefonico.Controllers
{
    public class AdministradorController:Controller
    {
        private readonly UserManager<SccUser> _user;
        public AdministradorController(UserManager<SccUser> user)
        {
            _user = user;
        }
        public async Task<ActionResult> Index(int pageNumber=1)
        {
            IQueryable<SccUser> users = _user.Users.Where(c=>c.UserName!=User.Identity.Name).AsQueryable();
            List<UserModel> usermodel= new List<UserModel>();
           
            foreach (SccUser user in users)
            {
                IEnumerable<string> roles = await _user.GetRolesAsync(user);
                Roles userRole = (Roles)Enum.Parse(typeof(Roles), roles.FirstOrDefault(), true);
                var userViewModel = Mapper.Map<UserModel>(user);
                user.NormalizedUserName= userRole.ToString();
                usermodel.Add(userViewModel);
            }
            IQueryable<UserModel> userModels = usermodel.ToList().AsQueryable(); 
            return View(await Paginacion<SccUser>.Create(users, pageNumber, 5));
          
        }
    }


}
