﻿using Catalogo_telefonico.Models;
using Catalogo_telefonico.Models.Enums;
using Catalogo_telefonico.Models.ViewModel;
using Catalogo_telefonico.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Catalogo_telefonico.Controllers
{
    public class AuthorizationController:Controller
    {
        private readonly UserManager<SccUser> _userManager;
        private readonly SignInManager<SccUser> _signInManager;
        private readonly AuthorizationService _auth;

        public AuthorizationController(
            UserManager<SccUser> userManager,
            SignInManager<SccUser> signInManager, AuthorizationService authorization)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _auth = authorization;
           

        }
        public async Task<ActionResult> Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (await _userManager.IsInRoleAsync(user, Roles.Administrador.ToString()))
                {
                    return RedirectToAction("Index", "Administrador");
                }
                if (await _userManager.IsInRoleAsync(user, Roles.Telefonico.ToString()) || await _userManager.IsInRoleAsync(user, Roles.supervisor.ToString()))
                {
                    return RedirectToAction("Index", "LineasMoviles");
                }
            }
            return View();
        }

        [HttpPost("login")]

        public async Task<ActionResult> login(LoginModelView login)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(login.UserName);
            var result=await _signInManager.PasswordSignInAsync(user, login.Password, false,false);
               

                if (result.Succeeded)

                {
                    if (await _userManager.IsInRoleAsync(user, Roles.Administrador.ToString()))
                    {
                        return RedirectToAction("Index", "Administrador");
                    }
                    if (await _userManager.IsInRoleAsync(user, Roles.Telefonico.ToString()) || await _userManager.IsInRoleAsync(user, Roles.supervisor.ToString()))
                    {
                        return RedirectToAction("Index", "LineasMoviles");
                    }
                }
                TempData["error"] = "Usuario o Contraseña incorrecta";

                return View();
            }
            return View(login);
          
        }
        public async Task<ActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("");
        }
    }
}
