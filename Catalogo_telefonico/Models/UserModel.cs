﻿using Catalogo_telefonico.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models
{
    public class UserModel
    {
        [Required]
        public string Id { get; set; }
        [Required, MaxLength(50)]
        public string name { get; set; }
        [Required, MaxLength(10)]
        public string telefono { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public TipoUser tipoUser { get; set; }
        [Required, Display(Name = "Rol")]
        public ERolesView roles { get; set; }
        public string password { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }
}
