﻿using Catalogo_telefonico.Models.Enums;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace Catalogo_telefonico.Models
{
    public class SccUser:IdentityUser
    {
        [Required, Display(Name = "Nombre de la persona"), MaxLength(50)]
        public string name { get; set; }
         [Required, MaxLength(10)]
        public string telefono { get; set; }
        [Required]
        public TipoUser tipoUser { get; set; }
    }
}
