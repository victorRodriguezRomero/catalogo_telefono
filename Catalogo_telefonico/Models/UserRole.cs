﻿using Microsoft.AspNetCore.Identity;

namespace Catalogo_telefonico.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public SccUser User { get; set; }

        public Role Role { get; set; }
    }
}
