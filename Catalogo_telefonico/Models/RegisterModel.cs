﻿using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models
{
    public class RegisterModel:CreateUserModel
    {
        [Required, Display(Name = "Contraseña"), MaxLength(15)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Las contraseñas no coinciden.")]
        [Required, Display(Name = "Confirmar Contraseña"), MaxLength(15)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
