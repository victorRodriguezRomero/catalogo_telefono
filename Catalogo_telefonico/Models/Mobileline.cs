﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models
{
    public partial class Mobileline
    {
        public Mobileline()
        {
            DetailMobilelines = new HashSet<DetailMobileline>();
        }
        [Required, Key]
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DetailMobileline> DetailMobilelines { get; set; }
    }
}
