﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Catalogo_telefonico.Models
{
    public partial class LineasContext:IdentityDbContext<SccUser>
    {

      
        public LineasContext(DbContextOptions<LineasContext> options)
            : base(options)
        {
        }
       

        public virtual DbSet<DetailMobileline> DetailMobilelines { get; set; }
        public virtual DbSet<Mobileline> Mobilelines { get; set; }
        public virtual DbSet<SccUser> user { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
           
        }

    }
}
