﻿namespace Catalogo_telefonico.Models
{
    public class ServiceResult<T> where T : class
    {
        public T Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        public ServiceResult(bool isSuccessful, T data = null, string message = "")
        {
            Success = isSuccessful;
            Message = message;
            Data = data;
        }
    }
}
