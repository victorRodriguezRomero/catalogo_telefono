﻿using Catalogo_telefonico.Models.Enums;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models
{
    public class CreateUserModel:IdentityUser 
    {
        [Required, Display(Name = "Nombre de la persona"), MaxLength(50)]
        public string name { get; set; }
        [Required, MaxLength(10)]
        public string telefono { get; set; }
        [Required]
        public TipoUser tipoUser { get; set; }
        [Required, Display(Name = "Rol")]
        public Roles roles { get; set; }
        public bool IsActive { get; set; }
    }
}
