﻿using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models.ViewModel
{
    public class LoginModelView
    {
        [Required, Display(Name = "Usuario"), MaxLength(15)]
        public string UserName { get; set; }

        [Required, Display(Name = "Contraseña"), MaxLength(15)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //Siempre se pedirá autenticación
        public bool RememberMe { get; } = false;
    }
}
