﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Catalogo_telefonico.Models
{
    public partial class DetailMobileline
    {
        [MaxLength(7), Required, Key]
        public int CallDetailId { get; set; }
        public string Phone { get; set; }
        public string CalledPartyNumber { get; set; }
        public string CalledPartyDescription { get; set; }
        public DateTime? DateTimeS { get; set; }
        public int? Duration { get; set; }
        public decimal? TotalCost { get; set; }

        public virtual Mobileline PhoneNavigation { get; set; }
    }
}
