using AutoMapper;
using Catalogo_telefonico.Models;
using Catalogo_telefonico.Repository;
using Catalogo_telefonico.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Catalogo_telefonico
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<LineasContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("MobileConect")));
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CreateUserModel, RegisterModel>().ReverseMap();
                cfg.CreateMap<RegisterModel, CreateUserModel>().ReverseMap();
                cfg.CreateMap<CreateUserModel, SccUser>().ReverseMap();
                cfg.CreateMap<UserModel, SccUser>().ReverseMap();
            });
            services.AddIdentity<SccUser, IdentityRole>(options =>
            {
                options.Lockout.AllowedForNewUsers = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
            })
               .AddEntityFrameworkStores<LineasContext>()
               .AddDefaultTokenProviders();
           
            services.AddTransient<SeedData>();

            services.AddScoped<UserRepository>();
            services.AddScoped<UserService>();
            services.AddScoped<AuthorizationService>();
            services.AddScoped<MobilelineServices>();
            services.AddMvc();
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SeedData seedData)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Authorization}/{action=Login}/{id?}");
            });
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<LineasContext>();
                context.Database.EnsureCreated();
            }

            seedData.Initialize().Wait();
        }
    }
}
