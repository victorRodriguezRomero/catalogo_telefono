﻿using Catalogo_telefonico.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
namespace Catalogo_telefonico
{
    public class SeedData
    {
            private RoleManager<IdentityRole> _roleManager;
            private UserManager<SccUser> _userManager;
           private LineasContext _context;

        public SeedData(RoleManager<IdentityRole> roleManager,
                        UserManager<SccUser> userManager, LineasContext context
                            )
            {
                _roleManager = roleManager;
                _userManager = userManager;
            _context = context;
            }

            public async Task Initialize()
            {
                await CreateRoles();
                await CreateUser();
                 //await CreateMobileline();
               
            }
        private async Task CreateRoles()
            {
                if (!await _roleManager.RoleExistsAsync("administrador"))
                {
                    await _roleManager.CreateAsync(new IdentityRole("administrador"));
                    await _roleManager.CreateAsync(new IdentityRole("Telefonico"));
                    await _roleManager.CreateAsync(new IdentityRole("supervisor"));
                }
            }

            private async Task CreateUser()
            {
                var adminUser = await _userManager.FindByNameAsync("administrador");

                if (adminUser == null)
                {
                   SccUser Administrador = new SccUser
                    {
                        name = "victor manuel",
                        UserName = "Administrador",
                        tipoUser =Models.Enums.TipoUser.interno,
                        telefono="6622247044"
                    };

                    var result = await _userManager.CreateAsync(Administrador, "123456");
                    Administrador = await _userManager.FindByNameAsync("Administrador");

                    result = await _userManager.AddToRolesAsync(Administrador, new string[]
                    {
                    "administrador",
                    "Telefonico",
                    "supervisor"
                    });
                }
            }
        }
    }

